package com.aqmoe.aquakits.exceptions;

public class InvalidLocalizationException extends RuntimeException {
    public InvalidLocalizationException(String message) {
        super(message);
    }
}
