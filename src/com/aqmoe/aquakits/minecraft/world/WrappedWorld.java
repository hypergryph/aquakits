package com.aqmoe.aquakits.minecraft.world;

import com.aqmoe.aquakits.minecraft.LowMinecraftVersionWarning;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

public class WrappedWorld {
    public World world;

    public World getWorld() {
        return world;
    }

    public WrappedWorld(World w) {
        world = w;
    }

    public Collection<Chunk> getForceLoadedChunks() {
        try {
            Method getForceLoadedChunks = world.getClass().getMethod("getForceLoadedChunks");
            Object result = getForceLoadedChunks.invoke(world);
            return (Collection<Chunk>) result;
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            return null;
        }
    }

    public Collection<Entity> getNearbyEntities(Location location, double x, double y, double z) throws UnsupportedOperationException {
        try {
            Method getNearbyEntities = world.getClass().getMethod("getNearbyEntities", Location.class, double.class, double.class, double.class);
            return (Collection<Entity>) getNearbyEntities.invoke(world, location, x, y, z);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            LowMinecraftVersionWarning.display();
            Collection<Entity> result = new ArrayList<>();
            Collection<Chunk> chunksNearby = WrappedChunk.around(location.getChunk(), 3, 3);
            for(Chunk chunk : chunksNearby) {
                for(Entity entity : chunk.getEntities()) {
                    Location entityLoc = entity.getLocation();
                    double max_distance = Math.max(Math.max(x, y), z);
                    if(entityLoc.distance(location) <= max_distance) {
                        result.add(entity);
                    }
                }
            }
            return result;
        }
    }
}
