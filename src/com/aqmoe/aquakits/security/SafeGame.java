package com.aqmoe.aquakits.security;

import org.bukkit.Material;

public class SafeGame {
    /**
     * 获得服务器是否是1.13及以后的新版物品命名空间
     *
     * 通过服务器 Material 枚举是否存在 GREEN_WOOL 来判断。
     * @return boolean
     */
    public static boolean getIsNewerVersion() {
        return (Material.getMaterial("GREEN_WOOL") != null);
    }
}
