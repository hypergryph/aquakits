package com.aqmoe.aquakits.security;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SafeInventoryEvent {
    public InventoryClickEvent event;
    public SafeInventoryEvent(InventoryClickEvent event1) {
        event = event1;
    }
    public String getTitle() {
        try {
            Object view = event.getClass().getMethod("getView").invoke(event);
            Method getTitle = view.getClass().getMethod("getTitle");
            getTitle.setAccessible(true);
            String title = (String) getTitle.invoke(view);
            return title;
        } catch (NoSuchMethodException e) {
            try {
                Method getInventory = event.getClass().getMethod("getInventory");
                getInventory.setAccessible(true);
                Object inventory = getInventory.invoke(event);
                Method getTitle = inventory.getClass().getMethod("getTitle");
                getTitle.setAccessible(true);
                return (String) getTitle.invoke(inventory);
            } catch (IllegalAccessException ex) {
                HonMeirin.hold(ex);
            } catch (InvocationTargetException ex) {
                HonMeirin.hold(ex);
            } catch (NoSuchMethodException ex) {
                HonMeirin.hold(ex);
            }
        } catch (InvocationTargetException e) {
            HonMeirin.hold(e);
        } catch (IllegalAccessException e) {
            HonMeirin.hold(e);
        }
        return null;
    }
}
