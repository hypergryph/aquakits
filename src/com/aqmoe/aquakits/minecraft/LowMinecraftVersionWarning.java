package com.aqmoe.aquakits.minecraft;

import com.aqmoe.aquakits.StringUtil;
import com.aqmoe.aquakits.language.Localization;
import com.aqmoe.aquakits.language.internal.InternalLanguageFields;
import org.bukkit.Bukkit;

public class LowMinecraftVersionWarning {
    public static boolean displayed = false;
    public static void display() {
        if(!displayed) Bukkit.getConsoleSender().sendMessage(StringUtil.tip(Localization.getTextBySystemLanguage(String.valueOf(InternalLanguageFields.LOW_PERFORMANCE_WARNING))));
        displayed = true;
    }
}
