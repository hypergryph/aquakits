package com.aqmoe.aquakits.language;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import com.aqmoe.aquakits.exceptions.InvalidLocalizationException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Locale;

public class Localization {
    /**
     * FieldID -> LocalizeField
     *            |- zh = "xxx"
     *            |- en = "xxx"
     */
    public static HashMap<String, TranslationField> localizeFieldHashMap = new HashMap<>();
    public static HashMap<String, String> namespaceDefaultLocale = new HashMap<>();

    public static void setNamespaceDefaultLocale(String namespace, String defaultLocale) {
        namespaceDefaultLocale.put(namespace, defaultLocale);
    }
    public static String getNamespaceDefaultLocale(String namespace) {
        return (namespaceDefaultLocale.containsKey(namespace)) ? namespaceDefaultLocale.get(namespace) : null;
    }

    /**
     * 向本地化库增加一条字段，并设置其翻译文本。
     * @param field_id 字段名称
     * @param translationField 翻译文本对象
     */
    public static void set(String field_id, TranslationField translationField) {
        field_id = field_id.toLowerCase(Locale.ROOT);
        localizeFieldHashMap.put(field_id, translationField);
    }

    public static void setTranslation(String field_id, String locale, String message) {
        if(getLocalizeField(field_id) != null) {
            TranslationField existsTF = getLocalizeField(field_id);
            existsTF.set(locale, message);
            set(field_id, existsTF);
        } else {
            TranslationField translationField = new TranslationField();
            translationField.set(locale, message);
            set(field_id, translationField);
        }
    }

    public static TranslationField getLocalizeField(String field_id) {
        field_id = field_id.toLowerCase(Locale.ROOT);
        return localizeFieldHashMap.get(field_id);
    }

    public static String getText(String locale, String field_id) {
        field_id = field_id.toLowerCase(Locale.ROOT);
        TranslationField translationField = getLocalizeField(field_id);
        if(translationField == null) {
            throw new InvalidLocalizationException("Text not found " + field_id);
        }
        return translationField.get(locale);
    }

    public static String getTextByPlayer(Player player, String field_id) {
        return getText(player.getLocale(), field_id);
    }

    public static String getTextBySystemLanguage(String field_id) {
        String namespace = (field_id.contains("_")) ? field_id.split("_")[0] : "default";
        if(getNamespaceDefaultLocale(namespace) != null) {
            return getText(getNamespaceDefaultLocale(namespace), field_id);
        }

        try {
            return getText(new Locale(System.getProperty("user.language"), System.getProperty("user.country")).toString(), field_id);
        } catch (Exception e) {
            if(Locale.getDefault() != null) {
                return getText(Locale.getDefault().toString(), field_id);
            }
            return getText("en", field_id);
        }

    }
}
