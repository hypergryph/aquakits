package com.aqmoe.aquakits.minecraft.itemstack;

import com.aqmoe.aquakits.exceptions.InvalidAquaMaterialException;
import org.bukkit.Material;

public class AquaMaterial {

    public Material bukkitMaterial;
    public AquaMaterial(String legacy_name, String new_name) {
        Material material = Material.getMaterial(legacy_name);
        Material material_new = Material.getMaterial(new_name);
        if(material != null) {
            bukkitMaterial = material;
        } else if (material_new != null) {
            bukkitMaterial = material_new;
        } else {
            throw new InvalidAquaMaterialException("legacy="+legacy_name+", new="+new_name+" could not be found on this version of game");
        }
    }

    public Material getBukkitMaterial() {
        return bukkitMaterial;
    }
}
