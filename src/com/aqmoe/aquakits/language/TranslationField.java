package com.aqmoe.aquakits.language;

import java.util.HashMap;
import java.util.Locale;

public class TranslationField {
    /**
     * zh => ""
     * en => ""
     */
    public HashMap<String, String> isoToString = new HashMap<>();
    private String formatLocale(String locale) {
        locale = locale.toLowerCase(Locale.ROOT);
        if(locale.contains("_")) {
            locale = locale.split("_")[0];
        }
        return locale;
    }
    public String get(String locale) {
        locale = formatLocale(locale);
        if(!isoToString.containsKey(locale)) {
            return get("en");
        }
        return isoToString.get(locale);
    }

    public TranslationField set(String locale, String text) {
        locale = formatLocale(locale);
        isoToString.put(locale, text);
        return this;
    }
}
