package com.aqmoe.aquakits.language.internal;

import com.aqmoe.aquakits.language.Localization;
import com.aqmoe.aquakits.language.TranslationField;

public class InternalLanguagePreset {
    public static void integrate() {
        Localization.set(String.valueOf(InternalLanguageFields.MR_PLAYER_EXCEPTION),
                new TranslationField().set("en", "Something unexpected happened. If you are administrator, please check the console for details.")
                        .set("zh", "抱歉，出现了一些问题。如果您是管理员，请到服务器后台查看详情。"));
        Localization.set(String.valueOf(InternalLanguageFields.MR_CONSOLE_EXCEPTION),
                new TranslationField().set("en", "We have encountered a problems that may cause plugin to crash.")
                        .set("zh", "抱歉，我们遇到了一个可能导致插件崩溃的问题。"));
        Localization.set(String.valueOf(InternalLanguageFields.MR_CONSOLE_EXCEPTION_PLEASE_REPORT),
                new TranslationField().set("en", "If possible, please upload the *entire* server log to the author (not only this exception), any bug reports are appreciated.")
                        .set("zh", "若有可能，请将 *整个* 服务器日志上传给作者分析（不只是这一个异常），我们对此感激不尽。"));
        Localization.set(String.valueOf(InternalLanguageFields.MR_IO_ERROR),
                new TranslationField().set("en", "This is an exception about file I/O. If you are using Linux, don't forget to give filesystem permissions to the process.")
                        .set("zh", "这是一个文件读写错误。如果正在使用Linux，请不要忘记给进程文件读写权限。"));
        Localization.set(String.valueOf(InternalLanguageFields.MR_LOCALIZATION_ERROR),
                new TranslationField().set("en", "The localization file of the plugin may be corrupted. You may repair this by deleting plugin's \"localizations\" folder and restart the server.")
                        .set("zh", "插件的语言文件可能已经损坏。遇到此错误，删除插件的 \"localizations\" 文件夹并重启服务器可能会有帮助。"));

        Localization.set(String.valueOf(InternalLanguageFields.ANS_SCHEDULER_ERROR),
                new TranslationField().set("en", "An exception has been detected from a timed task. To avoid log spamming, this task will be cancelled, which may result in broken plugin functionality.")
                        .set("zh", "一个定时任务出现了异常。为了避免日志刷屏，这个任务将被取消，但可能导致插件功能损坏。"));

        Localization.set(String.valueOf(InternalLanguageFields.LOW_PERFORMANCE_WARNING),
                new TranslationField().set("en", "The plugin is running at a very low Minecraft version and the performance of some operations may be degraded. If possible, please upgrade to a higher version.")
                        .set("zh", "插件运行在一个非常低的游戏版本下，部分操作的性能可能下降。如有可能，请升级到更高的游戏版本。"));
        Localization.set(String.valueOf(InternalLanguageFields.RENDERER_USE_MOUSE_NOT_SHIFT),
                new TranslationField().set("en", "To move items, please use mouse drag and drop.")
                        .set("zh", "若要移动物品，请使用鼠标拖拽。"));
    }
}
