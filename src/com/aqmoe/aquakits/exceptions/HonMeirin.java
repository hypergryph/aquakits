package com.aqmoe.aquakits.exceptions;

import com.aqmoe.aquakits.StringUtil;
import com.aqmoe.aquakits.language.Localization;
import com.aqmoe.aquakits.language.internal.InternalLanguageFields;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.IOException;

public class HonMeirin {
    public static void hold(Exception exception) {
        Bukkit.getConsoleSender().sendMessage(" ");
        Bukkit.getConsoleSender().sendMessage(StringUtil.error(Localization.getTextBySystemLanguage(String.valueOf(InternalLanguageFields.MR_CONSOLE_EXCEPTION))));
        Bukkit.getConsoleSender().sendMessage(StringUtil.error(Localization.getTextBySystemLanguage(String.valueOf(InternalLanguageFields.MR_CONSOLE_EXCEPTION_PLEASE_REPORT))));
        if(exception instanceof InvalidLocalizationException) {
            Bukkit.getConsoleSender().sendMessage(StringUtil.tip(Localization.getTextBySystemLanguage(String.valueOf(InternalLanguageFields.MR_LOCALIZATION_ERROR))));
        }
        if(exception instanceof IOException) {
            Bukkit.getConsoleSender().sendMessage(StringUtil.tip(Localization.getTextBySystemLanguage(String.valueOf(InternalLanguageFields.MR_IO_ERROR))));
        }
        exception.printStackTrace();
        Bukkit.getConsoleSender().sendMessage(" ");
    }
    public static void player_side(Exception exception, Player player) {
        player.sendMessage(StringUtil.error(Localization.getTextByPlayer(player, String.valueOf(InternalLanguageFields.MR_PLAYER_EXCEPTION))));
        player.sendMessage(StringUtil.error(exception.getClass().getName() + ": " + exception.getMessage()));
    }
}
