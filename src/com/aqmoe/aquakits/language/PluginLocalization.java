package com.aqmoe.aquakits.language;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PluginLocalization {
    /**
     * 从插件的 localization_[locale].yml 中读取语言文件
     * 保存该文件，并读取到 Localization 实例中。
     */
    public static void extractAndLoad(JavaPlugin currentPlugin, String namespace, String locale) {
        File file = new File(currentPlugin.getDataFolder(), "localization_" + locale + ".yml");
        File translation_dir = new File(currentPlugin.getDataFolder().getAbsolutePath() + "/translations/");
        File new_translation_file = new File(translation_dir, locale + ".yml");
        // 如果已经解压过了，就直接跳过解压步骤读取
        if (translation_dir.isDirectory() && new_translation_file.isFile()) {
            YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(new_translation_file);
            load(locale, yamlConfiguration, namespace);
            return;
        }
        currentPlugin.saveResource("localization_" + locale + ".yml", false);

        if (!translation_dir.isDirectory()) {
            translation_dir.mkdir();
        }
        Path localization_file = file.toPath();
        try {
            Files.move(localization_file, new_translation_file.toPath());
            YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(new_translation_file);
            load(locale, yamlConfiguration, namespace);
        } catch (IOException e) {
            HonMeirin.hold(e);
        }

    }

    public static void load(String locale, YamlConfiguration configuration, String namespace) {
        for (String key : configuration.getKeys(true)) {
            if (configuration.isString(key)) {
                // 如果该 key 是一个 string，那么这就是一个 language -> field -> message 模型了。
                Localization.setTranslation(namespace + "_" + key, locale, configuration.getString(key));
            }
        }

    }
}
