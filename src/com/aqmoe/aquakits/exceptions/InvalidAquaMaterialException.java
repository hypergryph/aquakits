package com.aqmoe.aquakits.exceptions;

public class InvalidAquaMaterialException extends RuntimeException {
    public InvalidAquaMaterialException(String message) {
        super(message);
    }
}
