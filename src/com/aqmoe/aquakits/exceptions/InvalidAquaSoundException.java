package com.aqmoe.aquakits.exceptions;

public class InvalidAquaSoundException extends RuntimeException {
    public InvalidAquaSoundException(String message) {
        super(message);
    }
}
