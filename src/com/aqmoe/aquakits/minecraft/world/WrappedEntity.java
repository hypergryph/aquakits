package com.aqmoe.aquakits.minecraft.world;

import de.tr7zw.nbtapi.NBTEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

public class WrappedEntity {
    public Entity bukkitEntity = null;
    public NBTEntity nbtEntity = null;

    public WrappedEntity(Entity entity) {
        bukkitEntity = entity;
    }

    public Entity getBukkitEntity() {
        return bukkitEntity;
    }

    public boolean nbtLoaded() {
        return (nbtEntity != null);
    }

    public void loadNBT() {
        nbtEntity = new NBTEntity(bukkitEntity);
    }

    public String getType() {
        if(!nbtLoaded()) loadNBT();
        if(isAvailable()) return null;
        return nbtEntity.getString("type");
    }

    public String getLowerCaseType() {
        return getType().toLowerCase(Locale.ROOT);
    }

    public boolean isAvailable() {
        return (!(bukkitEntity == null) && !bukkitEntity.isDead() && bukkitEntity.isValid() && !bukkitEntity.isEmpty() && nbtEntity != null);
    }

    public void setAI(boolean b) {
        if(!isLivingEntity()) return;
        try {
            Method setAI = LivingEntity.class.getMethod("setAI", boolean.class);
            setAI.invoke((LivingEntity) bukkitEntity, b);
        } catch (Exception e) {
            if(!nbtLoaded()) loadNBT();
            if(!isAvailable()) return;
            int noAI = (b) ? 1 : 0;
            nbtEntity.setInteger("NoAI", noAI);
        }

    }

    public String getCustomName() {
        try {
            Method getCustomName = Entity.class.getMethod("getCustomName");
            return (String) getCustomName.invoke(bukkitEntity);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isLivingEntity() {
        return (bukkitEntity instanceof LivingEntity);
    }

    public boolean hasAI() {
        if(!isLivingEntity()) return false;

        try {
            Method hasAI = LivingEntity.class.getMethod("hasAI");
            return (boolean) hasAI.invoke((LivingEntity) bukkitEntity);
        } catch (Exception e) {
            if(!nbtLoaded()) loadNBT();
            if(!isAvailable()) return false;
            if(nbtEntity.hasKey("NoAI")) {
                if(nbtEntity.getInteger("NoAI") == 1) {
                    return false;
                }
            }
            return true;
        }

    }
}
