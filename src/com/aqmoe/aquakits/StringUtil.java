package com.aqmoe.aquakits;

public class StringUtil {
    public static String color(String m) {
        m = m.replace("&", "§");
        return m;
    }
    public static String progress(String m) {
        return color(" &7[&8/&7] &r&7" + m);
    }
    public static String done(String m) {
        return color(" &7[&a√&7] &r&2" + m);
    }
    public static String error(String m) {
        return color(" &7[&4×&7] &r&c" + m);
    }
    public static String tip(String m) {
        return color(" &7[&6!&7] &r&6" + m);
    }
}
