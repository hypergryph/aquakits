package com.aqmoe.aquakits.scheduler;

import com.aqmoe.aquakits.StringUtil;
import com.aqmoe.aquakits.exceptions.HonMeirin;
import com.aqmoe.aquakits.language.Localization;
import com.aqmoe.aquakits.language.internal.InternalLanguageFields;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class ANormalScheduler {
    String _name;
    String _description;
    Runnable _runnable;
    ScheduleType _type;
    Integer _delay;
    Integer _period;
    BukkitTask _task;

    public ANormalScheduler(String name, String description, Runnable runnable, ScheduleType type, Integer delay, Integer period) {
        _name = name;
        _description = description;
        _runnable = createSafeRunnable(runnable, this);
        _type = type;
        _delay = delay;
        _period = period;
    }

    @Override
    public String toString() {
        return "ANormalScheduler{" +
                "name='" + _name + '\'' +
                ", description='" + _description + '\'' +
                ", type=" + _type.name() +
                ", delay=" + _delay +
                ", period=" + _period +
                '}';
    }

    public Runnable createSafeRunnable(Runnable runnable, ANormalScheduler instance) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } catch (Exception e) {
                    Bukkit.getConsoleSender().sendMessage(StringUtil.tip(Localization.getTextBySystemLanguage(String.valueOf(InternalLanguageFields.ANS_SCHEDULER_ERROR))));
                    Bukkit.getConsoleSender().sendMessage(StringUtil.tip("Scheduler instance: " + instance.toString()));
                    if(instance != null) {
                        instance.cancel();
                    }
                    HonMeirin.hold(e);
                }
            }
        };
    }

    public String getDescription() {
        return _description;
    }

    public Integer getDelay() {
        return _delay;
    }

    public Integer getPeriod() {
        return _period;
    }

    public ScheduleType getType() {
        return _type;
    }

    public Runnable getRunnable() {
        return _runnable;
    }

    public String getName() {
        return _name;
    }

    public BukkitTask getTask() {
        return _task;
    }

    public void setTask(BukkitTask task) {
        this._task = task;
    }

    public void setDescription(String description) {
        this._description = description;
    }

    public void setName(String name) {
        this._name = name;
    }

    public void start(JavaPlugin plugin) {
        try {

            // 在注册 task 前检查插件是否是 enable 状态!!
            if(!plugin.isEnabled()) {
                return;
            }

            BukkitTask task;
            switch (getType()) {
                case SYNC:
                    task = Bukkit.getScheduler().runTask(plugin, getRunnable());
                    break;
                case LATER:
                    task = Bukkit.getScheduler().runTaskLater(plugin, getRunnable(), getDelay());
                    break;
                case TIMER:
                    task = Bukkit.getScheduler().runTaskTimer(plugin, getRunnable(), getDelay(), getPeriod());
                    break;
                case LATER_ASYNC:
                    task = Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, getRunnable(), getDelay());
                    break;
                case TIMER_ASYNC:
                    task = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, getRunnable(), getDelay(), getPeriod());
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + getType());
            }
            setTask(task);
        } catch (Exception e) {
            plugin.getLogger().severe("Failed to start scheduler " + getName() + "!");
            HonMeirin.hold(e);
        }
    }

    public void cancel() {
        if(_task != null && !_task.isCancelled()) {
            _task.cancel();
        }
    }
}
