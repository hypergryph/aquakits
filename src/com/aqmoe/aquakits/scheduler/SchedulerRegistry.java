package com.aqmoe.aquakits.scheduler;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;

public class SchedulerRegistry {
    public static HashMap<String, ANormalScheduler> schedulerMap = new HashMap<>();
    public static void registerScheduler(ANormalScheduler scheduler) {
        schedulerMap.put(scheduler.getName(), scheduler);
    }
}
