package com.aqmoe.aquakits.scheduler;

public enum ScheduleType {
    TIMER,
    TIMER_ASYNC,
    LATER,
    LATER_ASYNC,
    SYNC
}
