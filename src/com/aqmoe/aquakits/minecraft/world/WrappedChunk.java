package com.aqmoe.aquakits.minecraft.world;

import org.bukkit.Chunk;
import org.bukkit.World;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class WrappedChunk {
    public Chunk chunk;
    public WrappedChunk(Chunk c) {
        chunk = c;
    }

    public Chunk getChunk() {
        return chunk;
    }

    public boolean supportForceLoadedChunks() {
        try {
            Method setForceLoaded = chunk.getClass().getMethod("setForceLoaded", boolean.class);
            return true;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    @Override
    public String toString() {
        return getChunk().getX() + "," + getChunk().getZ();
    }

    public boolean setForceLoaded(boolean b) {
        try {
            Method setForceLoaded = chunk.getClass().getMethod("setForceLoaded", boolean.class);
            setForceLoaded.invoke(chunk, b);
            return true;
        } catch (NoSuchMethodException e) {
            return false;
        } catch (InvocationTargetException e) {
            return false;
        } catch (IllegalAccessException e) {
            return false;
        }
    }

    public boolean isForceLoaded() {
        try {
            Method setForceLoaded = chunk.getClass().getMethod("isForceLoaded");
            return (boolean) setForceLoaded.invoke(chunk);
        } catch (NoSuchMethodException e) {
            return false;
        } catch (InvocationTargetException e) {
            return false;
        } catch (IllegalAccessException e) {
            return false;
        }
    }

    public static Collection<Chunk> around(Chunk origin, int radius_x, int radius_z) {
        World world = origin.getWorld();

        int length_x = (radius_x * 2) + 1;
        int length_y = (radius_z * 2) + 1;
        Set<Chunk> chunks = new HashSet<>(length_x * length_y);

        int cX = origin.getX();
        int cZ = origin.getZ();

        for (int x = -radius_x; x <= radius_x; x++) {
            for (int z = -radius_z; z <= radius_z; z++) {
                chunks.add(world.getChunkAt(cX + x, cZ + z));
            }
        }
        return chunks;
    }
}
