package com.aqmoe.aquakits.security;

import org.bukkit.Bukkit;

public class SafeConfig {
    public static Integer expectingInteger(Integer data, Integer min, Integer max, String description) {
        if(data > max || data < min) {
            Bukkit.getLogger().warning(description);
        }
        return data;
    }

    public static Integer divisionRounding(Integer i1, Integer i2) {
        double result = (i1 / i2);
        double rounded = Math.ceil(result);
        return new Double(rounded).intValue();
    }
}
