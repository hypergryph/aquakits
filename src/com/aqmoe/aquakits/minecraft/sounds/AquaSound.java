package com.aqmoe.aquakits.minecraft.sounds;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import com.aqmoe.aquakits.exceptions.InvalidAquaSoundException;
import com.aqmoe.aquakits.security.SafeGame;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AquaSound {
    public Method playSound = null;
    public boolean playAvailable = false;
    public Sound sound;
    public AquaSound(String legacy_name, String new_name){
        try {
            if(Player.class.getMethod("playSound", Location.class, Sound.class, float.class, float.class) != null) {
                playSound = Player.class.getMethod("playSound", Location.class, Sound.class, float.class, float.class);
                playAvailable = true;
            }
        } catch (NoSuchMethodException e) {
            playAvailable = false;
        }
        if(SafeGame.getIsNewerVersion()) {
            sound = Sound.valueOf(new_name);
        } else {
            sound = Sound.valueOf(legacy_name);
        }

        if(sound == null) {
            throw new InvalidAquaSoundException("sound legacy="+legacy_name+", new="+new_name+" could not be found on this version of game");
        }
    }

    public void play(Player player) {
        if(playAvailable) {
            try {
                playSound.invoke(player.getLocation(), sound, 1F, 1F);
            } catch (IllegalAccessException e) {
                HonMeirin.hold(e);
                HonMeirin.player_side(e, player);
            } catch (InvocationTargetException e) {
                HonMeirin.hold(e);
                HonMeirin.player_side(e, player);
            }
        }
    }
}
