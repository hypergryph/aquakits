package com.aqmoe.aquakits.language.internal;

public enum InternalLanguageFields {
    MR_PLAYER_EXCEPTION("MR_PLAYER_EXCEPTION"),
    MR_CONSOLE_EXCEPTION("MR_CONSOLE_EXCEPTION"),
    MR_CONSOLE_EXCEPTION_PLEASE_REPORT("MR_CONSOLE_EXCEPTION_PLEASE_REPORT"),
    MR_LOCALIZATION_ERROR("MR_LOCALIZATION_ERROR"),
    MR_IO_ERROR("MR_IO_ERROR"),
    ANS_SCHEDULER_ERROR("ANS_SCHEDULER_ERROR"),
    LOW_PERFORMANCE_WARNING("WRAPPED_WORLD_LOW_PERFORMANCE_WARNING"),
    RENDERER_USE_MOUSE_NOT_SHIFT("RENDERER_USE_MOUSE_NOT_SHIFT");



    final String label;

    InternalLanguageFields(String s) {
        label = s;
    }
}
