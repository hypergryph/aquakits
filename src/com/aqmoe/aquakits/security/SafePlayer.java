package com.aqmoe.aquakits.security;

import com.aqmoe.aquakits.exceptions.UnsafeOperationException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

public class SafePlayer {
    public static Player toPlayer(CommandSender commandSender) throws UnsafeOperationException {
        if(isPlayer(commandSender)) {
            return (Player) commandSender;
        } else {
            throw new UnsafeOperationException("Call toPlayer() without confirming if it's a Player!");
        }
    }
    public static boolean isPlayer(CommandSender commandSender) {
        if(commandSender instanceof Player) {
            return true;
        } else {
            return false;
        }
    }

    public static Player toPlayer(HumanEntity humanEntity) {
        if(humanEntity instanceof Player) {
            return (Player) humanEntity;
        } else {
            return null;
        }
    }
}
