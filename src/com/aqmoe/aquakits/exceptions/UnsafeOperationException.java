package com.aqmoe.aquakits.exceptions;

public class UnsafeOperationException extends RuntimeException {
    public UnsafeOperationException(String message) {
        super(message);
    }
}
