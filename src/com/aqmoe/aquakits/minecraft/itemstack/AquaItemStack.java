package com.aqmoe.aquakits.minecraft.itemstack;

import java.lang.reflect.Constructor;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import com.aqmoe.aquakits.exceptions.UnsafeOperationException;
import com.aqmoe.aquakits.security.SafeGame;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class AquaItemStack {
    public ItemStack bukkitItemStack = null;
    public AquaItemStack(AquaMaterial type, int amount, short extra) {
        if(SafeGame.getIsNewerVersion()) {
            try {
                Constructor constructor = ItemStack.class.getConstructor(Material.class, int.class);
                bukkitItemStack = (ItemStack) constructor.newInstance(type.getBukkitMaterial(), amount);
            } catch (Exception e) {
                HonMeirin.hold(e);
            }
        } else {
            try {
                Constructor constructor = ItemStack.class.getConstructor(Material.class, int.class, short.class);
                bukkitItemStack = (ItemStack) constructor.newInstance(type.getBukkitMaterial(), amount, extra);
            } catch (Exception e) {
                HonMeirin.hold(e);
            }
        }
    }

    public ItemStack getBukkitItemStack() {
        if(bukkitItemStack == null) {
            throwNotPrepared();
        }
        return bukkitItemStack;
    }

    public void throwNotPrepared() {
        try {
            throw new UnsafeOperationException("bukkitItemStack not prepared!");
        } catch (UnsafeOperationException e) {
            HonMeirin.hold(e);
        }

    }

    public boolean prepared() {
        return (bukkitItemStack != null);
    }
}
