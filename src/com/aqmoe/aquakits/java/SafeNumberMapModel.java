package com.aqmoe.aquakits.java;

import com.aqmoe.aquakits.scheduler.ANormalScheduler;
import com.aqmoe.aquakits.scheduler.ScheduleType;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class SafeNumberMapModel {
    public ConcurrentHashMap<String, AtomicInteger> map;
    public SafeNumberMapModel(int scheduled_clear_seconds, JavaPlugin plugin) {
        map = new ConcurrentHashMap<>();
        if(scheduled_clear_seconds > 0) {
            new ANormalScheduler(
                    "SafeNumberMapModelScheduler",
                    "Do scheduled clearing",
                    new Runnable() {
                        @Override
                        public void run() {
                            clear();
                        }
                    },
                    ScheduleType.TIMER_ASYNC,
                    20,
                    (20 * scheduled_clear_seconds)
            ).start(plugin);
        }
    }
    public SafeNumberMapModel() {
        map = new ConcurrentHashMap<>();
    }
    public void set(String key, Integer initial_value) {
        AtomicInteger integer = new AtomicInteger(initial_value);
        map.put(key, integer);
    }
    public Integer get(String key) {
        if (!map.containsKey(key)) {
            map.put(key, new AtomicInteger(0));
        }
        return map.get(key).get();
    }
    public void add(String key, Integer delta) {
        AtomicInteger integer = map.get(key);
        if(integer == null) {
            integer = new AtomicInteger(0);
        }
        integer.addAndGet(delta);
        map.put(key, integer);
    }
    public void remove(String key) {
        map.remove(key);
    }
    public ConcurrentHashMap.KeySetView<String, AtomicInteger> keySet() {
        return map.keySet();
    }
    public void clear() {
        map.clear();
    }
}
